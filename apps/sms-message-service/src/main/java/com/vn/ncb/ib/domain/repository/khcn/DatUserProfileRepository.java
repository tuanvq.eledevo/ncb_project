//package com.vn.ncb.ib.domain.repository.khcn;
//
//import com.vn.ncb.ib.domain.entity.khcn.DatUserProfile;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.Optional;
//
//@Repository
//public interface DatUserProfileRepository extends JpaRepository<DatUserProfile, String> {
//	Optional<DatUserProfile> findByUsrid(String usrid);
//}
