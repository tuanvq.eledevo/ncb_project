package com.vn.ncb.ib.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({ "classpath:database-config.properties" })
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.vn.ncb.ib.domain.repository.khcn",
		entityManagerFactoryRef = "khcnEntityManager",
		transactionManagerRef = "khcnTransactionManager"
)
public class DatasourceKHCNConfig {
	private final Environment env;

	public DatasourceKHCNConfig(Environment env) {
		this.env = env;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean khcnEntityManager() {
		LocalContainerEntityManagerFactoryBean em
				= new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(khcnDataSource());
		em.setPackagesToScan("com.vn.ncb.ib.domain.entity.khcn");

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		em.setJpaPropertyMap(properties);

		return em;
	}

	@Bean
	public DataSource khcnDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.khcn-datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.khcn-datasource.url"));
		dataSource.setUsername(env.getProperty("spring.khcn-datasource.username"));
		dataSource.setPassword(env.getProperty("spring.khcn-datasource.password"));

		return dataSource;
	}

	@Bean
	public PlatformTransactionManager khcnTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(khcnEntityManager().getObject());
		return transactionManager;
	}
}
