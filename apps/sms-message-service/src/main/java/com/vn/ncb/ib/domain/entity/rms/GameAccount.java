//package com.vn.ncb.ib.domain.entity.rms;
//
//import lombok.*;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import java.sql.Date;
//
//@Entity
//@Table(name = "GAME_ACCOUNT", schema = "RPTNCB")
//@Getter
//@Setter
//@ToString
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//public class GameAccount {
//
//	@Id
//	@Column(name = "ID", nullable = false)
//	private Long id;
//
//	@Column(name = "EFF_DATE")
//	private String effDate;
//
//	@Column(name = "CUSTOMER")
//	private String customer;
//
//	@Column(name = "SHORT_TITLE")
//	private String shortTitle;
//
//	@Column(name = "CURRENCY")
//	private String currency;
//
//	@Column(name = "OPEN_ACTUAL_BAL")
//	private String openActualBal;
//
//	@Column(name = "TYPE")
//	private String type;
//
//	@Column(name = "BODY_TYPE")
//	private String bodyType;
//
//	@Column(name = "TYPE_ACCOUNT")
//	private String typeAccount;
//
//	@Column(name = "DATE_ACCOUNT")
//	private Date dateAccount;
//
//	@Column(name = "SCAN_JOB_STATUS")
//	private Integer scanJobStatus;
//}
