//package com.vn.ncb.ib.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//
//@Configuration
//@PropertySource({"classpath:database-config.properties"})
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        basePackages = "com.vn.ncb.ib.domain.repository.rms",
//        entityManagerFactoryRef = "rmsEntityManager",
//        transactionManagerRef = "rmsTransactionManager"
//)
//public class DatasourceRMSConfig {
//    private final Environment env;
//
//    public DatasourceRMSConfig(Environment env) {
//        this.env = env;
//    }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean rmsEntityManager() {
//        LocalContainerEntityManagerFactoryBean em
//                = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(rmsDataSource());
//        em.setPackagesToScan("com.vn.ncb.ib.domain.entity.rms");
//
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        em.setJpaVendorAdapter(vendorAdapter);
//        HashMap<String, Object> properties = new HashMap<>();
//        properties.put("hibernate.hbm2ddl.auto",env.getProperty("spring.jpa.hibernate.ddl-auto"));
//        properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
//        em.setJpaPropertyMap(properties);
//
//        return em;
//    }
//
//    @Bean
//    public DataSource rmsDataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(env.getProperty("spring.rms-datasource.driver-class-name"));
//        dataSource.setUrl(env.getProperty("spring.rms-datasource.url"));
//        dataSource.setUsername(env.getProperty("spring.rms-datasource.username"));
//        dataSource.setPassword(env.getProperty("spring.rms-datasource.password"));
//
//        return dataSource;
//    }
//
//    @Bean
//    public PlatformTransactionManager rmsTransactionManager() {
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(rmsEntityManager().getObject());
//        return transactionManager;
//    }
//}
