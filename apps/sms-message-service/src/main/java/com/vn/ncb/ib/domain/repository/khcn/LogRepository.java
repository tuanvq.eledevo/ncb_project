package com.vn.ncb.ib.domain.repository.khcn;

import com.vn.ncb.ib.domain.entity.khcn.CustomerEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface LogRepository extends JpaRepository<CustomerEvent, UUID> {
}
