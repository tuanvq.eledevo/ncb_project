package com.vn.ncb.ib.domain.entity.khcn;

import lombok.*;
import oracle.sql.TIMESTAMP;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "USER_IB_EVENTS", schema = "DUY")
public class UserIbEvent {
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    private UUID userIbId;

    @Column(name="DATA")
    private String data;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name="OTP_PHONE")
    private String otpPhone;

    @Column(name = "CUSTOMER")
    private String customer;

    @Column(name = "CUSTOMER_TYPE")
    private String customerType;

    @Column(name="SMS_STATUS")
    private String smsStatus;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name = "MODIFIED_DATE")
    private TIMESTAMP modifiedDate;

    @Column(name = "CREATED_DATE")
    private  TIMESTAMP createdDate;
}
