package com.vn.ncb.ib.domain.entity.khcn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {
    private String customer;
    private String sector;
    private String phoneNumber;
    private ArrayList<String> legalDocName = new ArrayList<>();
    private ArrayList<String> legalId =new ArrayList<>();
}
