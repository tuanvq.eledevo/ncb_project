package com.vn.ncb.ib.config;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

@Configuration
public class RestTemplateConfig {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate(Objects.requireNonNull(trustCert()));
	}

	private ClientHttpRequestFactory trustCert() {
		ClientHttpRequestFactory requestFactory;
		SSLContext sslContext = getSSlContext();
		if (null == sslContext) {
			return null;
		}
		HttpClientBuilder closeableClientBuilder = HttpClientBuilder.create();
		// Add the SSLContext and trust manager
		closeableClientBuilder.setSSLContext(getSSlContext());
		// add the hostname verifier
		closeableClientBuilder.setSSLHostnameVerifier(gethostnameVerifier());
		requestFactory = new HttpComponentsClientHttpRequestFactory(closeableClientBuilder.build());
		return requestFactory;
	}

	private static SSLContext getSSlContext() {
		final TrustManager[] trustAllCerts = new TrustManager[] { getTrustManager() };
		SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			e.printStackTrace();
		}
		return sslContext;
	}

	private static X509TrustManager getTrustManager() {

		return new X509TrustManager() {

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
		};
	}

	private static HostnameVerifier gethostnameVerifier() {
		return (arg0, arg1) -> true;
	}
}
