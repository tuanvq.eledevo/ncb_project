package com.vn.ncb.ib.constant;

public enum ScanJobStatus {

	NOT_SCAN(0, "NOT_SCAN"),
	SCAN_SUCCESS(1, "SCAN_SUCCESS"),
	SCAN_FAIL(2, "SCAN_FAIL");

	private final Integer code;

	private final String value;

	ScanJobStatus(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public Integer code() {
		return code;
	}

	public String value() {
		return value;
	}
}
