package com.vn.ncb.ib.domain.entity.khcn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import oracle.sql.TIMESTAMP;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CUSTOMER", schema = "DUY")
@Data
public class Customer {
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    private UUID Id;

    @Column(name="CUSTOMER_ID")
    private String customerId;


    @Column(name = "LEGAL_DOC_NAME")
    private String legalDocName;

    @Column(name="LEGAL_DOC_ID")
    private String legalDocId;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CREATED_DATE")
    private  Date createdDate;
}

