package com.vn.ncb.ib.domain.entity.khcn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "CUSTOMER_EVENTS", schema = "DUY")
@Data
public class CustomerEvent {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private UUID customerEventId;

    @Column(name="dataCustomerEvent")
    private String dataCustomerEvent;

    @Column(name = "CUSTOMER_ID")
    private String customerId;

    @Column(name = "SMS_STATUS")
    private String smsStatus;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name = "SECTOR")
    private String sector;

    @Column(name = "SMS_1")
    private String sms;

    @Column(name = "LEGAL_DOC_NAME")
    private String legalDocName;

    @Column(name = "LEGAL_ID")
    private String legalID;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CREATED_DATE")
    private  Date createdDate;

}

