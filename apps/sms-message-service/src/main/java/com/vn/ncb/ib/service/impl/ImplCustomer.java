package com.vn.ncb.ib.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vn.ncb.ib.domain.entity.khcn.Customer;
import com.vn.ncb.ib.domain.entity.khcn.CustomerEvent;
import com.vn.ncb.ib.domain.entity.khcn.CustomerRequest;
import com.vn.ncb.ib.domain.repository.khcn.CustomerRepository;
import com.vn.ncb.ib.domain.repository.khcn.LogRepository;
import com.vn.ncb.ib.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ImplCustomer implements CustomerService {
    private static final String status_1 = "DONE";
    private static final String status_2 = "FAILURE";
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private LogRepository logRepository;

    @Override
    public String postCustomer(CustomerRequest customerRequest) {
        try {
//            ObjectMapper objectMapper = new ObjectMapper();
//            String customer = objectMapper.writeValueAsString(customerRequest);
//            System.out.println(customer + "is" + "customer");
//            CustomerEvent customerEvent = new CustomerEvent();
//            customerEvent.setDataCustomerEvent(customer);
//            logRepository.save(customerEvent);
//            addCustomer(customerRequest);
            List<Customer> customerList = customerRepository.findAllByCustomerId(customerRequest.getCustomer());
            System.out.println(customerList);
            if (customerList.size() < 1) {
                System.out.println("thi add vao db");
            } else {
                String arr1 = customerList.get(0).getLegalDocId();
                ArrayList<String> arr2 = customerRequest.getLegalId();
                System.out.println(arr1 + "listDB");
                System.out.println(arr2 + "listRQ");
                String arr3 = arr2.toString();
                System.out.println(arr3 + "listchange");
                Boolean test = checkArraysEquality(arr1, arr3);
                System.out.println(test);
                if (customerList.size() >= 2) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    String dataCustomer = objectMapper.writeValueAsString(customerRequest);
                    CustomerEvent cusEvent = CustomerEvent.builder()
                            .dataCustomerEvent(dataCustomer)
                            .customerId(customerRequest.getCustomer())
                            .smsStatus(status_1)
                            .description("Tồn tại trên 1 bản ghi")
                            .sector(customerRequest.getSector())
                            .sms(customerRequest.getPhoneNumber())
                            .legalDocName(customerRequest.getLegalDocName().toString())
                            .legalID(customerRequest.getLegalId().toString())
                            .build();
                    logRepository.save(cusEvent);
                    log.info("Data created successfully");
                }
                char a = '1';
                System.out.println(customerRequest.getSector().charAt(0) == a);
                if (customerRequest.getSector().charAt(0) == a && !test) {
                    String text = cut(arr3);
                    String contentMessage = "Quy khach dang yeu cau thay doi so Giay to tuy than sang so " + text + ".Neu khong phai yeu cau cua QK vui long lien he tong dai 18006166 de duoc ho tro";
                    System.out.println(contentMessage);
                }
            }
            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void checkDataCustomer(CustomerRequest customerRequest) {
        try {

        } catch (Exception e) {

        }
    }

    private void addCustomer(CustomerRequest customerRequest) {
        try {
            Customer customer = Customer.builder()
                    .customerId(customerRequest.getCustomer())
                    .legalDocId(customerRequest.getLegalId().toString())
                    .legalDocName(customerRequest.getLegalDocName().toString())
                    .phoneNumber(customerRequest.getPhoneNumber())
                    .build();
            customerRepository.save(customer);
            log.info("Data created successfully", customer);
        } catch (Exception ex) {
            log.info("Err", ex);
        }
    }

    private boolean checkArraysEquality(String arr1, String arr2) {
        if (arr1 == null || arr2 == null) {
            return false;
        }
        return arr1.equals(arr2);
    }

    private String cut(String arr1) {
        if (StringUtils.hasText(arr1)) {
            String number = arr1.replaceAll("\\D+", ""); // Loại bỏ tất cả các ký tự không phải là số
            String firstThreeDigits = number.substring(0, 3);
            String lastThreeDigits = number.substring(number.length() - 3);
            String text = firstThreeDigits + "****" + lastThreeDigits;
            return text;
        }
        return null;
    }
}
