package com.vn.ncb.ib.controller;

import com.vn.ncb.ib.domain.entity.khcn.CustomerRequest;
import com.vn.ncb.ib.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @PostMapping(value = "/post")
    public ResponseEntity<?> addCustomer (@RequestBody CustomerRequest customerRequest){
        return new  ResponseEntity<>(customerService.postCustomer(customerRequest),HttpStatus.OK);
    }
}
