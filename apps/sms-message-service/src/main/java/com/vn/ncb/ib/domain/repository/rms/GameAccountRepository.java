//package com.vn.ncb.ib.domain.repository.rms;
//
//import com.vn.ncb.ib.domain.entity.rms.GameAccount;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Date;
//import java.util.List;
//
//@Repository
//public interface GameAccountRepository extends JpaRepository<GameAccount, Long> {
//	@Query(value = "select a from GameAccount a where a.scanJobStatus = :status and a.dateAccount = :dateAccount")
//	List<GameAccount> getGameAccountByStatus(Integer status, Date dateAccount, Pageable pageable);
//
//	@Modifying
//	@Transactional
//	@Query(value = "update GameAccount a set a.scanJobStatus = :status where a.id in :ids and a.dateAccount = :dateAccount")
//	void updateScanJobStatusByIds(Integer status, List<Long> ids, Date dateAccount);
//}
