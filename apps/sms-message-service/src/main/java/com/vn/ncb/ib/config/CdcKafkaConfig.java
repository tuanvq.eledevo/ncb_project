package com.vn.ncb.ib.config;

import lombok.var;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CdcKafkaConfig {

	@Value(value = "${spring.kafka.cdc-consumer.bootstrap-servers}")
	private String cdcBootstrapServers;

	@Value(value = "${spring.kafka.cdc-consumer.group-id}")
	private String cdcConsumerGroupId;

	@Value("${spring.kafka.cdc.path-key-store}")
	public String pathKeyStore;

	@Value("${spring.kafka.cdc.pass-key-store}")
	public String passKeyStore;

	@Value("${spring.kafka.cdc.path-trust-store}")
	public String pathTrustStore;

	@Value("${spring.kafka.cdc.pass-trust-store}")
	public String passTrustStore;

	@Bean
	@Qualifier("cdcConsumerFactory")
	public ConsumerFactory<String, String> cdcConsumerFactory() {
		return new DefaultKafkaConsumerFactory<>(
				cdcConsumerProperties(),
				new StringDeserializer(),
				new StringDeserializer()
		);
	}

	private Map<String, Object> cdcConsumerProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, cdcBootstrapServers);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, cdcConsumerGroupId);
		props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");

		//SSL
		props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
		props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, pathTrustStore);
		props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, passTrustStore);
		props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, pathKeyStore);
		props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, passKeyStore);
		props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, passKeyStore);
		props.put(SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
		return props;
	}

	@Bean
	@Qualifier("cdcListenerContainerFactory")
	public ConcurrentKafkaListenerContainerFactory<String, String> cdcListenerContainerFactory(
			@Qualifier("cdcConsumerFactory") ConsumerFactory<String, String> cdcConsumerFactory
	) {
		var factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
		factory.setConsumerFactory(cdcConsumerFactory);
		return factory;
	}
}
