//package com.vn.ncb.ib.listener;
//
//import com.vn.ncb.ib.service.CdcService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Component
//public class CdcListener {
//
//	private final CdcService cdcService;
//
//	public CdcListener(CdcService cdcService) {
//		this.cdcService = cdcService;
//	}
//
//	@KafkaListener(
//			topics = { "${spring.kafka.topics.cdc-saoke-topics}" },
//			containerFactory = "cdcListenerContainerFactory",
//			concurrency = "${spring.kafka.request-consumer.concurrency}",
//			autoStartup = "${spring.kafka.request-consumer.enable:true}"
//	)
//	public void handleSaokeCDCMessage(String message) {
//		long start = System.currentTimeMillis();
//		try {
//			log.info("handleSaokeCDCMessage START");
//			cdcService.handleSaokeCdcMessage(message);
//		} catch (Exception ex) {
//			log.error("Error while handling saoke cdc message", ex);
//		} finally {
//			log.info("process_saoke_cdc_request{process_time=" + (System.currentTimeMillis() - start) + "ms}");
//			log.info("handleSaokeCDCMessage END");
//		}
//	}
//
//	@KafkaListener(
//			topics = { "${spring.kafka.topics.cdc-ekyc-topics}" },
//			containerFactory = "cdcListenerContainerFactory",
//			concurrency = "${spring.kafka.request-consumer.concurrency}",
//			autoStartup = "${spring.kafka.request-consumer.enable:true}"
//	)
//	public void handleEkycCDCMessage(String message) {
//		long start = System.currentTimeMillis();
//		try {
//			log.info("handleEkycCDCMessage START");
//			cdcService.handleEkycCdcMessage(message);
//		} catch (Exception ex) {
//			log.error("Error while handling ekyc cdc message", ex);
//		} finally {
//			log.info("process_ekyc_cdc_request{process_time=" + (System.currentTimeMillis() - start) + "ms}");
//			log.info("handleEkycCDCMessage END");
//		}
//	}
//
//	@KafkaListener(
//			topics = { "${spring.kafka.topics.cdc-card-topics}" },
//			containerFactory = "cdcListenerContainerFactory",
//			concurrency = "${spring.kafka.request-consumer.concurrency}",
//			autoStartup = "${spring.kafka.request-consumer.enable:true}"
//	)
//	public void handleCardCDCMessage(String message) {
//		long start = System.currentTimeMillis();
//		try {
//			log.info("handleCardCDCMessage START");
//			cdcService.handleCardCdcMessage(message);
//		} catch (Exception ex) {
//			log.error("Error while handling card cdc message", ex);
//		} finally {
//			log.info("process_card_cdc_request{process_time=" + (System.currentTimeMillis() - start) + "ms}");
//			log.info("handleCardCDCMessage END");
//		}
//	}
//}
