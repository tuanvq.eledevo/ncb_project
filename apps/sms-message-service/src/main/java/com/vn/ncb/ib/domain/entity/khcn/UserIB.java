package com.vn.ncb.ib.domain.entity.khcn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import oracle.sql.TIMESTAMP;

import javax.persistence.*;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "USER_IB", schema = "DUY")
@Data
public class UserIB {
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    private UUID userId;

    @Column(name="CUSTOMER_ID")
    private String customerId;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name="OTP_PHONE_NUMBER")
    private String otpPhoneNumber;

    @Column(name = "MODIFIED_DATE")
    private TIMESTAMP modifiedDate;

    @Column(name = "CREATED_DATE")
    private  TIMESTAMP createdDate;

}





