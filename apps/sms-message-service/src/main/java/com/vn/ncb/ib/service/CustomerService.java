package com.vn.ncb.ib.service;

import com.vn.ncb.ib.domain.entity.khcn.CustomerRequest;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    String postCustomer (CustomerRequest customerRequest);
    void checkDataCustomer(CustomerRequest customerRequest);
}
