package com.vn.ncb.ib.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ping")
public class Ping {

	@RequestMapping(value = {"/", ""})
    public String ping() {
        return "Ready";
    }

}
