package com.vn.ncb.common.model.cdc;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CdcEvent<T> {
	private PayloadCdc<T> payload;
}
