package com.vn.ncb.common.model;

import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class NotificationEvent {
	private String notificationType;
	private String notificationTitle;
	private String notificationBody;
	private List<String> customers;
	private String amount;
}
