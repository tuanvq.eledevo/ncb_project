package com.vn.ncb.common.model.cdc;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EkycIssueCdc {
	private String MSGID;
	private String FULL_NAME;
	private String LEGAL_ID;
	private String OLD_LEGAL_ID;
	private String LEGAL_DATE;
	private String GENDER;
	private String MOBILE_NUMBER;
	private String EMAIL;
	private String COMPANY_CODE;
	private String DEPARTMENT_CODE;
	private String USER_NAME;
	private String STATUS;
	private String CREATE_TIME;
	private String UPDATE_TIME;
	private String BIRTHDAY;
	private String PLACE_OF_ORIGIN;
	private String PLACE_OF_RESIDENCE;
	private String ISSUSE_DATE;
	private String EXPIRED_DATE;
	private String ISSUSE_PLACE;
	private String NATIONALITY;
	private String NATION;
	private String RELIGION;
	private String IMAGE_NAME;
	private String ADDRESS;
}
