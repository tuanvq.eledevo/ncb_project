package com.vn.ncb.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcbIbApplication {
	public static void main(String[] args) {
		SpringApplication.run(NcbIbApplication.class, args);
	}
}
