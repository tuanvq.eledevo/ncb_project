package com.vn.ncb.common.exception;

public class JsonException extends RuntimeException {
    public JsonException(Throwable cause) {
        super(cause);
    }
}
