package com.vn.ncb.common.util;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Slf4j
public class DateTimeUtil {

	private DateTimeUtil() {
	}

	public static String getCurrentDate(String sFormat) {
		DateFormat sdf = new SimpleDateFormat(sFormat);
		Date curDate = new Date();
		try {
			return sdf.format(curDate);
		} catch (NullPointerException | IllegalArgumentException ex) {
			log.error(ex.getMessage(), ex);
			return null;
		}
	}
}
