package com.vn.ncb.common.model.cdc;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CardIssueCdc {
	private String ID;
	private String MSGID;
	private String CIFNO;
	private String USER_NAME;
	private String FULL_NAME;
	private String CARD_NO;
	private String TYPE_CARD;
	private String TYPE_ISSUE;
	private String TYPE_FORM;
	private String ACCOUNT;
	private String FEE_AMOUNT;
	private String FEE_CODE;
	private String FEE_CODE_T24;
	private String CURRENCY;
	private String CARD_PRODUCT_PHYSICAL;
	private String CARD_PRODUCT_VIRTUAL;
	private String ADDRESS_RECEIVE;
	private String COMPANY_CODE_RECEIVE;
	private String DATE_TIME;
	private String STATUS;
	private String ERROR_DETAIL;
}
