package com.vn.ncb.common.model;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GameFoxEvent<T> {

	private String eventType;
	private T eventBody;

	public String toStringEventData() {
		return "GameFoxEvent{" +
				"eventType='" + eventType + '\'' +
				"eventBody='" + eventBody.toString() + '\'' +
				'}';
	}
}
