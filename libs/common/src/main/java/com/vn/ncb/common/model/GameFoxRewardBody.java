package com.vn.ncb.common.model;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GameFoxRewardBody {
	private String customerId;
	private String rewardType;
	private String rewardName;
	private String amount;
	private String timestamp;
	private String appType;
}
