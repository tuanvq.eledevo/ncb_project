package com.vn.ncb.common.model.cdc;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PayloadCdc<T> {
	private String table;
	private String op_type;
	private T after;
}
