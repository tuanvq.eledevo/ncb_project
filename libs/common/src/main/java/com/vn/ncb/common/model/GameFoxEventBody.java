package com.vn.ncb.common.model;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GameFoxEventBody {
	private String id;
	private String type;
	private String customerId;
	private String amount;
	private String timestamp;
}
