package com.vn.ncb.common.model.cdc;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SaokeMbappCdc {
	private String ID;
	private String SYSTEM_ID;
	private String COMPANY;
	private String DAO;
	private String CUSTOMER;
	private String BOOKING_DATE;
	private String DATE_TIME;
	private String TRANS_REFERENCE;
	private String BALANCE_BEGIS;
	private Double AMT;
	private String DC_TYPE;
	private String BALANCE_END;
	private String CCY;
	private String NARRATIVE;
	private String STATUS_T24;
	private String STMT_ID;
	private String DATE_GD;
	private String TIME_GD;
	private String TRANS_TYPE;
	private String TRANS_TYPE_NAME;
	private String TYPE_ID;
	private String CIF_DEBIT;
	private String ACCT1;
	private String ACCT1_NAME;
	private String ACCT2;
	private String ACCT2_NAME;
	private String CITAD_CODE;
	private String BEN_BANK;
	private String FEE;
	private String TRACE;
	private String CODE_FEE;
	private String KENH_GD;
	private String STATUS_GD;
	private String ERROR_DETAIL;
	private String PARTNER;
	private String BILL_NO;
	private String VERSION;
	private String FEE_METHOD;
	private String RECEIVING_ADDR;
	private String VANGLAI_CMND;
	private String VANGLAI_CMND_OPEN;
	private String VANGLAI_CMND_PLACE;
	private String TERM;
	private String LIQ_ACCT;
	private String KY_LINHLAI;
	private String PRDCODE;
	private String RATE;
	private String AMT_DISCOUNT;
	private String ROLL_PRIN;
	private String ROLL_INT;
	private String PAYIN_ACCT;
	private String PAYOUT_ACCT;
	private String UNIQ_ID;
	private String MSGID;
	private String CITAD_CODE_TT;
	private String BEN_BANK_NAME;
	private String BEN_PROVINCE_NAME;
	private String BEN_BRANCH_CODE;
	private String BEN_BRANCH_NAME;
	private String BILL_INF;
	private String MERCHANT_NAME;
	private String MERCHANT_NO;
	private String DUE_BILL_DATE;
	private String BEN_ACCT;
	private String BEN_NAME;
	private String BILL_DATE;
	private String USER_ID;
	private String BUT_TOAN;
	private String INTSPREAD;
	private String REASON_CANCEL;
	private String STATUS_GD_PREVIOUS;
	private String APPROVAL_LEVEL;
	private String CHECKER_NAME;
	private String CHECK_DATE_TIME;
	private String APPROVER_NAME;
	private String APPROVAL_DATE_TIME;
	private String TERM_COMMIT;
	private String EWALLET_INFOR;
	private String CONFIRM_TYPE;
	private String DISCOUNT_CODE;
	private String SOURCE_COMMIT;
	private String SOURCE_OPTYPE;
	private String SOURCE_SCN;
	private String TARGET_COMMIT;
}
