package com.vn.ncb.common.constant;

public class Constant {

	private Constant() {
	}

	public static final String MB_CHANNEL = "MB";
	public static final String CURRENCY_VND = "VND";

	public static class GameReward {
		private GameReward() {
		}

		public static final String CONGRATULATION_MESSAGE_1 = "Thuong chuong trinh";
		public static final String CONGRATULATION_MESSAGE_2 = " Gap rong vang 2024";
	}

	public static class OperationType {
		private OperationType() {
		}

		public static final String CREATE = "C";
		public static final String INSERT = "I";
		public static final String UPDATE = "U";
	}

}
