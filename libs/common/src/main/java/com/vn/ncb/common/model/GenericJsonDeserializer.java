package com.vn.ncb.common.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vn.ncb.common.exception.JsonException;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class GenericJsonDeserializer<T> implements Deserializer<T> {
	private final ObjectMapper objectMapper;
	private final TypeReference<T> typeReference;

	public GenericJsonDeserializer(ObjectMapper objectMapper, TypeReference<T> typeReference) {
		this.objectMapper = objectMapper;
		this.typeReference = typeReference;
	}

	@Override
	public T deserialize(String topic, byte[] data) {
		if (data == null) return null;
		String s = new String(data, StandardCharsets.UTF_8);
		try {
			return objectMapper.readValue(s, typeReference);
		} catch (JsonProcessingException e) {
			throw new JsonException(e);
		}
	}
}
