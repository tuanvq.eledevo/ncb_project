package com.vn.ncb.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonUtil {

	private CommonUtil() {
	}

	public static boolean isNullOrEmpty(String str) {
		return str == null || str.isEmpty();
	}

	public static boolean nonNullOrEmpty(String text) {
		return text != null && !text.isEmpty();
	}

	public static List<String> convertStringToList(String data) {
		return new ArrayList<>(Arrays.asList(data.split(",")));
	}
}
