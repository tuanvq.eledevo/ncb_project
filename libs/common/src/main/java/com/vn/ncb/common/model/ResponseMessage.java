package com.vn.ncb.common.model;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseMessage {

	private String code;
	private Object body;
	private String description;
}